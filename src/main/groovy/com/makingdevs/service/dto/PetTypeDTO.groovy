package com.makingdevs.service.dto

import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString
import io.micronaut.core.annotation.Introspected

@Introspected
@EqualsAndHashCode
@ToString
class PetTypeDTO implements Serializable {
  Long id
  String name
}
