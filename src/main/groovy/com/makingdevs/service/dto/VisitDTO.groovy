package com.makingdevs.service.dto

import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString
import io.micronaut.core.annotation.Introspected

import java.time.LocalDate

@Introspected
@EqualsAndHashCode
@ToString
class VisitDTO implements Serializable{
  Long id
  LocalDate visitDate
  String description
  Long petId
}
