package com.makingdevs.service.dto

import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString
import io.micronaut.core.annotation.Introspected

@Introspected
@EqualsAndHashCode
@ToString
class OwnerDTO implements Serializable{
  Long id
  String firstName
  String lastName
  String address
  String city
  String telephone
  Set<PetDTO> pets = new HashSet<>()
}
