package com.makingdevs.service.dto

import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString
import io.micronaut.core.annotation.Introspected

import java.time.LocalDate

@Introspected
@EqualsAndHashCode
@ToString
class PetDTO implements Serializable{
  Long id
  String name
  LocalDate birthDate
  Set<VisitDTO> visits = new HashSet<>()
  PetTypeDTO type
  Long ownerId
}
