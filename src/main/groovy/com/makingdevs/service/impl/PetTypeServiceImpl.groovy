package com.makingdevs.service.impl

import com.makingdevs.domain.PetType
import com.makingdevs.repository.PetTypeRepository
import com.makingdevs.service.PetTypeService
import com.makingdevs.service.dto.PetTypeDTO
import com.makingdevs.service.mapper.PetTypeMapper
import groovy.util.logging.Slf4j
import io.micronaut.data.model.Page
import io.micronaut.data.model.Pageable
import io.micronaut.transaction.annotation.ReadOnly
import jakarta.inject.Inject
import jakarta.inject.Singleton

import javax.transaction.Transactional

@Slf4j
@Singleton
@Transactional
class PetTypeServiceImpl implements PetTypeService {

  private final PetTypeRepository petTypeRepository

  PetTypeServiceImpl(PetTypeRepository petTypeRepository){
    this.petTypeRepository = petTypeRepository
  }

  @Inject
  PetTypeMapper petTypeMapper

  @Override
  PetTypeDTO save(PetTypeDTO petTypeDTO) {
    log.info("Request to save PetType: ${petTypeDTO}")
    petTypeMapper.toDto(petTypeRepository.mergeAndSave(petTypeMapper.toEntity(petTypeDTO).get())).get()
  }

  @Override
  @ReadOnly
  @Transactional
  Page<PetTypeDTO> findAll(Pageable pageable) {
    log.info("Request to get all PetTypes")
    petTypeMapper.toDtoList(petTypeRepository.findAll(pageable).getContent())
  }

  @Override
  @ReadOnly
  @Transactional
  Optional<PetTypeDTO> findOne(Long id) {
    log.info("Request to get PetType: ${id}")
    Optional<PetType> optionalPetType = petTypeRepository.findById(id)
    optionalPetType.isPresent() ? petTypeMapper.toDto(optionalPetType.get()) : null
  }

  @Override
  void delete(Long id) {
    log.info("Request to delete PetType: ${id}")
    petTypeRepository.deleteById(id)
  }
}
