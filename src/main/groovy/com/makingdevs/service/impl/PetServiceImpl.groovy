package com.makingdevs.service.impl

import com.makingdevs.domain.Pet
import com.makingdevs.repository.PetRepository
import com.makingdevs.service.PetService
import com.makingdevs.service.dto.PetDTO
import com.makingdevs.service.mapper.PetMapper
import groovy.util.logging.Slf4j
import io.micronaut.data.model.Page
import io.micronaut.data.model.Pageable
import io.micronaut.transaction.annotation.ReadOnly
import jakarta.inject.Inject
import jakarta.inject.Singleton

import javax.transaction.Transactional

@Slf4j
@Singleton
@Transactional
class PetServiceImpl implements PetService{

  @Inject
  PetRepository petRepository
  @Inject
  PetMapper petMapper

  @Override
  PetDTO save(PetDTO petDTO) {
    log.info("Request to save Pet: ${petDTO}")
    petMapper.toDto(petRepository.mergeAndSave(petMapper.toEntity(petDTO).get())).get()
  }

  @Override
  @ReadOnly
  @Transactional
  Page<PetDTO> findAll(Pageable pageable) {
    log.info("Request to get all Pets")
    petMapper.toDtoList(petRepository.findAll(pageable).getContent())
  }

  @Override
  @ReadOnly
  @Transactional
  Optional<PetDTO> findOne(Long id) {
    log.info("Request to get Pet: ${id}")
    Optional<Pet> optionalPet = petRepository.findById(id)
    optionalPet.isPresent() ? petMapper.toDto(optionalPet.get()) : null
  }

  @Override
  void delete(Long id) {
    log.info("Request to delete Pet: ${id}")
    petRepository.deleteById(id)
  }
}
