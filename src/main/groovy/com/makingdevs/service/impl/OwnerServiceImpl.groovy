package com.makingdevs.service.impl

import com.makingdevs.domain.Owner
import com.makingdevs.repository.OwnerRepository
import com.makingdevs.service.OwnerService
import com.makingdevs.service.dto.OwnerDTO
import com.makingdevs.service.mapper.OwnerMapper
import groovy.util.logging.Slf4j
import io.micronaut.data.model.Page
import io.micronaut.data.model.Pageable
import io.micronaut.transaction.annotation.ReadOnly
import jakarta.inject.Inject
import jakarta.inject.Singleton

import javax.transaction.Transactional

@Slf4j
@Singleton
@Transactional
class OwnerServiceImpl implements OwnerService{

  @Inject
  OwnerRepository ownerRepository
  @Inject
  OwnerMapper ownerMapper

  @Override
  OwnerDTO save(OwnerDTO ownerDTO) {
    log.info("Request to save Owner: ${ownerDTO}")
    ownerMapper.toDto(ownerRepository.mergeAndSave(ownerMapper.toEntity(ownerDTO).get())).get()
  }

  @Override
  @ReadOnly
  @Transactional
  Page<OwnerDTO> findAll(Pageable pageable) {
    log.info("Request to get all Owners")
    ownerMapper.toDtoList(ownerRepository.findAll(pageable).getContent())
  }

  @Override
  @ReadOnly
  @Transactional
  Optional<OwnerDTO> findOne(Long id) {
    log.info("Request to get Owner: ${id}")
    Optional<Owner> optionalOwner = ownerRepository.findById(id)
    optionalOwner.isPresent() ? ownerMapper.toDto(optionalOwner.get()) : null
  }

  @Override
  void delete(Long id) {
    log.info("Request to delete Owner: ${id}")
    ownerRepository.deleteById(id)
  }
}
