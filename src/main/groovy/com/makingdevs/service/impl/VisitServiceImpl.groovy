package com.makingdevs.service.impl

import com.makingdevs.domain.Visit
import com.makingdevs.repository.VisitRepository
import com.makingdevs.service.VisitService
import com.makingdevs.service.dto.VisitDTO
import com.makingdevs.service.mapper.VisitMapper
import groovy.util.logging.Slf4j
import io.micronaut.data.model.Page
import io.micronaut.data.model.Pageable
import io.micronaut.transaction.annotation.ReadOnly
import jakarta.inject.Inject
import jakarta.inject.Singleton

import javax.transaction.Transactional

/**
 * Service Implementation for managing {@link Visit}
 */
@Slf4j
@Singleton
@Transactional
class VisitServiceImpl implements VisitService{

  @Inject
  VisitRepository visitRepository

  @Inject
  VisitMapper visitMapper

  /**
   * Save a visit.
   *
   * @param visit
   * @return
   */
  @Override
  VisitDTO save(VisitDTO visitDTO) {
    log.info("Request to save Visit: ${visitDTO}")
    visitMapper.toDto(visitRepository.mergeAndSave(visitMapper.toEntity(visitDTO).get())).get()
  }

  @Override
  @ReadOnly
  @Transactional
  Page<VisitDTO> findAll(Pageable pageable) {
    log.info("Request to get all Visits")
    visitMapper.toDtoList(visitRepository.findAll(pageable).getContent())
  }

  @Override
  @ReadOnly
  @Transactional
  Optional<VisitDTO> findOne(Long id) {
    log.info("Request to get Visit: ${id}")
    Optional<Visit> optionalVisit = visitRepository.findById(id)
    optionalVisit.isPresent() ? visitMapper.toDto(optionalVisit.get()) : null
  }

  @Override
  void delete(Long id) {
    log.info("Request to delete Visit: ${id}")
    visitRepository.deleteById(id)
  }
}
