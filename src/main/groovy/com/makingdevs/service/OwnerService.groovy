package com.makingdevs.service

import com.makingdevs.service.dto.OwnerDTO
import io.micronaut.data.model.Page
import io.micronaut.data.model.Pageable

interface OwnerService {

  OwnerDTO save(OwnerDTO ownerDTO)

  Page<OwnerDTO> findAll(Pageable pageable)

  Optional<OwnerDTO> findOne(Long id)

  void delete(Long id)

}