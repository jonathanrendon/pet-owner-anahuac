package com.makingdevs.service

import com.makingdevs.service.dto.PetDTO
import io.micronaut.data.model.Page
import io.micronaut.data.model.Pageable

interface PetService {

  PetDTO save(PetDTO petDTO)

  Page<PetDTO> findAll(Pageable pageable)

  Optional<PetDTO> findOne(Long id)

  void delete(Long id)

}