package com.makingdevs.service

import com.makingdevs.service.dto.PetTypeDTO
import io.micronaut.data.model.Page
import io.micronaut.data.model.Pageable

interface PetTypeService {

  PetTypeDTO save(PetTypeDTO petTypeDTO)

  Page<PetTypeDTO> findAll(Pageable pageable)

  Optional<PetTypeDTO> findOne(Long id)

  void delete(Long id)

}