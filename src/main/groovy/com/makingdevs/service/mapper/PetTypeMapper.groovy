package com.makingdevs.service.mapper

import com.makingdevs.domain.PetType
import com.makingdevs.service.dto.PetTypeDTO
import groovy.util.logging.Slf4j
import io.micronaut.data.model.Page
import io.micronaut.data.model.Pageable
import jakarta.inject.Singleton

/**
 * Mapper for entity {@link PetType} and its DTO {@link PetTypeDTO}
 */
@Slf4j
@Singleton
class PetTypeMapper implements EntityMapper<PetTypeDTO, PetType>{

  @Override
  Optional<PetType> toEntity(PetTypeDTO dto) {
    PetType entity = new PetType()
    entity.id = dto.id
    entity.name = dto.name
    Optional.of(entity)
  }

  @Override
  Optional<PetTypeDTO> toDto(PetType entity) {
    PetTypeDTO dto = new PetTypeDTO()
    dto.id = entity.id
    dto.name = entity.name
    Optional.of(dto)
  }

  @Override
  Page<PetType> toEntityList(List<PetTypeDTO> dtoList) {
    List<PetType> entityList = new ArrayList<PetType>()
    dtoList.collect {dto ->
      PetType entity = new PetType()
      entity.id = dto.id
      entity.name = dto.name
      entityList.add(entity)
    }
    Page.of(entityList,Pageable.UNPAGED,entityList.size())
  }

  @Override
  Page<PetTypeDTO> toDtoList(List<PetType> entityList) {
    List<PetTypeDTO> dtoList = new ArrayList<PetTypeDTO>()
    entityList.collect { entity ->
      PetTypeDTO dto = new PetTypeDTO()
      dto.id = entity.id
      dto.name = entity.name
      dtoList.add(dto)
    }
    Page.of(dtoList,Pageable.UNPAGED,dtoList.size())
  }
}
