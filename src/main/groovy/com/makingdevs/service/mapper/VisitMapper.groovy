package com.makingdevs.service.mapper

import com.makingdevs.domain.Pet
import com.makingdevs.domain.Visit
import com.makingdevs.service.dto.VisitDTO
import groovy.util.logging.Slf4j
import io.micronaut.data.model.Page
import io.micronaut.data.model.Pageable
import jakarta.inject.Singleton

@Slf4j
@Singleton
class VisitMapper implements EntityMapper<VisitDTO, Visit>{

  @Override
  Optional<Visit> toEntity(VisitDTO dto) {
    Visit entity = new Visit()
    Pet pet = new Pet()
    entity.pet = pet
    entity.id = dto.id
    entity.description = dto.description
    entity.visitDate = dto.visitDate
    entity.pet.id = dto.petId
    Optional.of(entity)
  }

  @Override
  Optional<VisitDTO> toDto(Visit entity) {
    VisitDTO dto = new VisitDTO()
    dto.id = entity.id
    dto.description = entity.description
    dto.visitDate = entity.visitDate
    dto.petId = entity.pet.id
    Optional.of(dto)
  }

  @Override
  Page<Visit> toEntityList(List<VisitDTO> dtoList) {
    List<Visit> entityList = new ArrayList<>()
    dtoList.collect {dto ->
      Visit entity = new Visit()
      Pet pet = new Pet()
      entity.pet = pet
      entity.id = dto.id
      entity.description = dto.description
      entity.visitDate = dto.visitDate
      entity.pet.id = dto.petId
      entityList.add(entity)
    }
    Page.of(entityList, Pageable.UNPAGED,entityList.size())
  }

  @Override
  Page<VisitDTO> toDtoList(List<Visit> entityList) {
    List<VisitDTO> dtoList = new ArrayList<VisitDTO>()
    entityList.collect { entity ->
      VisitDTO dto = new VisitDTO()
      dto.id = entity.id
      dto.description = entity.description
      dto.visitDate = entity.visitDate
      dto.petId = entity.pet.id
      dtoList.add(dto)
    }
    Page.of(dtoList, Pageable.UNPAGED, dtoList.size())
  }
}
