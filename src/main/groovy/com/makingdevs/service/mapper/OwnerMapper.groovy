package com.makingdevs.service.mapper

import com.makingdevs.domain.Owner
import com.makingdevs.service.dto.OwnerDTO
import groovy.util.logging.Slf4j
import io.micronaut.data.model.Page
import io.micronaut.data.model.Pageable
import jakarta.inject.Inject
import jakarta.inject.Singleton

@Slf4j
@Singleton
class OwnerMapper implements EntityMapper<OwnerDTO, Owner> {

  @Inject
  PetMapper petMapper

  @Override
  Optional<Owner> toEntity(OwnerDTO dto) {
    Owner entity = new Owner()
    entity.id = dto.id
    entity.firstName = dto.firstName
    entity.lastName = dto.lastName
    entity.address = dto.address
    entity.city = dto.city
    entity.telephone = dto.telephone
    entity.pets = petMapper.toEntityList(dto.pets.collect()).getContent().toSet()
    Optional.of(entity)
  }

  @Override
  Optional<OwnerDTO> toDto(Owner entity) {
    OwnerDTO dto = new OwnerDTO()
    dto.id = entity.id
    dto.firstName = entity.firstName
    dto.lastName = entity.lastName
    dto.address = entity.address
    dto.city = entity.city
    dto.telephone = entity.telephone
    dto.pets = petMapper.toDtoList(entity.pets.collect()).getContent().toSet()
    Optional.of(dto)
  }

  @Override
  Page<Owner> toEntityList(List<OwnerDTO> dtoList) {
    List<Owner> entityList = new ArrayList<Owner>()
    dtoList.collect {dto ->
      Owner entity = new Owner()
      entity.id = dto.id
      entity.firstName = dto.firstName
      entity.lastName = dto.lastName
      entity.address = dto.address
      entity.city = dto.city
      entity.telephone = dto.telephone
      entity.pets = petMapper.toEntityList(dto.pets.collect()).getContent().toSet()
      entityList.add(entity)
    }
    Page.of(entityList, Pageable.UNPAGED, entityList.size())
  }

  @Override
  Page<OwnerDTO> toDtoList(List<Owner> entityList) {
    List<OwnerDTO> dtoList = new ArrayList<OwnerDTO>()
    entityList.collect {entity ->
      OwnerDTO dto = new OwnerDTO()
      dto.id = entity.id
      dto.firstName = entity.firstName
      dto.lastName = entity.lastName
      dto.address = entity.address
      dto.city = entity.city
      dto.telephone = entity.telephone
      dto.pets = petMapper.toDtoList(entity.pets.collect()).getContent().toSet()
      dtoList.add(dto)
    }
    Page.of(dtoList, Pageable.UNPAGED, dtoList.size())
  }
}
