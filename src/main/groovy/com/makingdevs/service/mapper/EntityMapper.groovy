package com.makingdevs.service.mapper

import io.micronaut.data.model.Page

trait EntityMapper <D,E>{
  abstract Optional<E> toEntity(D dto)
  abstract Optional<D> toDto(E entity)
  abstract Page<E> toEntityList(List<D> dtoList)
  abstract Page<D> toDtoList(List<E> entityList)
}