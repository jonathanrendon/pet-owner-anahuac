package com.makingdevs.service.mapper

import com.makingdevs.domain.Owner
import com.makingdevs.domain.Pet
import com.makingdevs.service.dto.PetDTO
import groovy.util.logging.Slf4j
import io.micronaut.data.model.Page
import io.micronaut.data.model.Pageable
import jakarta.inject.Inject
import jakarta.inject.Singleton

@Slf4j
@Singleton
class PetMapper implements EntityMapper<PetDTO, Pet>{

  @Inject
  VisitMapper visitMapper
  @Inject
  PetTypeMapper petTypeMapper

  @Override
  Optional<Pet> toEntity(PetDTO dto) {
    Pet entity = new Pet()
    Owner owner = new Owner()
    entity.owner = owner
    entity.id = dto.id
    entity.name = dto.name
    entity.birthDate = dto.birthDate
    entity.visits = visitMapper.toEntityList(dto.visits.collect()).getContent().toSet()
    entity.type = petTypeMapper.toEntity(dto.type).get()
    entity.owner.id = dto.ownerId
    Optional.of(entity)
  }

  @Override
  Optional<PetDTO> toDto(Pet entity) {
    PetDTO dto = new PetDTO()
    dto.id = entity.id
    dto.name = entity.name
    dto.birthDate = entity.birthDate
    dto.visits = visitMapper.toDtoList(entity.visits.collect()).getContent().toSet()
    dto.type = petTypeMapper.toDto(entity.type).get()
    dto.ownerId = entity.owner.id
    Optional.of(dto)
  }

  @Override
  Page<Pet> toEntityList(List<PetDTO> dtoList) {
    List<Pet> entityList = new ArrayList<Pet>()
    dtoList.collect {dto ->
      Pet entity = new Pet()
      Owner owner = new Owner()
      entity.owner = owner
      entity.id = dto.id
      entity.name = dto.name
      entity.birthDate = dto.birthDate
      entity.visits = visitMapper.toEntityList(dto.visits.collect()).getContent().toSet()
      entity.type = petTypeMapper.toEntity(dto.type).get()
      entity.owner.id = dto.ownerId
      entityList.add(entity)
    }
    Page.of(entityList, Pageable.UNPAGED, entityList.size())
  }

  @Override
  Page<PetDTO> toDtoList(List<Pet> entityList) {
    List<PetDTO> dtoList = new ArrayList<PetDTO>()
    entityList.collect {entity ->
      PetDTO dto = new PetDTO()
      dto.id = entity.id
      dto.name = entity.name
      dto.birthDate = entity.birthDate
      dto.visits = visitMapper.toDtoList(entity.visits.collect()).getContent().toSet()
      dto.type = petTypeMapper.toDto(entity.type).get()
      dto.ownerId = entity.owner.id
      dtoList.add(dto)
    }
    Page.of(dtoList, Pageable.UNPAGED, dtoList.size())
  }
}
