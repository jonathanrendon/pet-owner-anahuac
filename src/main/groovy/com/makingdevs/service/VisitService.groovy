package com.makingdevs.service

import com.makingdevs.domain.Visit
import com.makingdevs.service.dto.VisitDTO
import io.micronaut.data.model.Page
import io.micronaut.data.model.Pageable


interface VisitService {

  VisitDTO save(VisitDTO visitDTO)

  Page<VisitDTO> findAll(Pageable pageable)

  Optional<VisitDTO> findOne(Long id)

  void delete(Long id)

}