package com.makingdevs.web.rest

import com.makingdevs.service.VisitService
import com.makingdevs.service.dto.VisitDTO
import com.makingdevs.utils.HeaderUtil
import com.makingdevs.utils.PaginationUtil
import com.makingdevs.web.rest.errors.BadRequestAlertException
import groovy.util.logging.Slf4j
import io.micronaut.context.annotation.Value
import io.micronaut.data.model.Page
import io.micronaut.data.model.Pageable
import io.micronaut.http.HttpRequest
import io.micronaut.http.HttpResponse
import io.micronaut.http.annotation.Body
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Delete
import io.micronaut.http.annotation.Get
import io.micronaut.http.annotation.PathVariable
import io.micronaut.http.annotation.Post
import io.micronaut.http.annotation.Put
import io.micronaut.http.uri.UriBuilder
import io.micronaut.security.annotation.Secured
import io.micronaut.security.rules.SecurityRule
import io.swagger.v3.oas.annotations.tags.Tag
import jakarta.inject.Inject

@Tag(name = "Visit")
@Slf4j
@Controller("/api")
class VisitResource {

  private static final String ENTITY_NAME = "visit"

  @Value('${micronaut.application.name}')
  private String applicationName

  @Inject
  VisitService visitService

  @Post("/visits")
  HttpResponse<VisitDTO> createVisit(@Body VisitDTO visitDTO) throws URISyntaxException {
    log.info("REST request to save Visit : ${visitDTO}")
    if (visitDTO.getId()) {
      throw new BadRequestAlertException("A new visit cannot already have an ID", ENTITY_NAME, "idexists")
    }
    VisitDTO result = visitService.save(visitDTO)
    URI location = new URI("/api/visits/${result.getId()}")
    HttpResponse.created(result).headers(headers -> {
      headers.location(location)
      HeaderUtil.createEntityCreationAlert(headers, applicationName, true, ENTITY_NAME, result.getId().toString())
    })
  }

  @Put("/visits")
  HttpResponse<VisitDTO> updateVisit(@Body VisitDTO visitDTO) throws URISyntaxException {
    log.info("REST request to update Visit : ${visitDTO}")
    if (visitDTO.getId() == null) {
      throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull")
    }
    VisitDTO result = visitService.save(visitDTO)
    HttpResponse.ok(result).headers(headers ->
            HeaderUtil.createEntityUpdateAlert(headers, applicationName, true, ENTITY_NAME, visitDTO.getId().toString()))
  }

  @Get("/visits")
  HttpResponse<List<VisitDTO>> getAllVisits(HttpRequest request, Pageable pageable) {
    log.info("REST request to get a page of Visits")
    Page<VisitDTO> page = visitService.findAll(pageable)
    log.info("Size of all visits: ${page.getContent().size()}")
    HttpResponse.ok(page.getContent()).headers(headers ->
            PaginationUtil.generatePaginationHttpHeaders(headers, UriBuilder.of(request.getPath()), page))
  }

  @Get("/visits/{id}")
  Optional<VisitDTO> getVisit(@PathVariable Long id) {
    log.info("REST request to get Visit : ${id}")
    return visitService.findOne(id)
  }

  @Delete("/visits/{id}")
  HttpResponse deleteVisit(@PathVariable Long id) {
    log.info("REST request to delete Visit : ${id}")
    visitService.delete(id)
    HttpResponse.noContent().headers(headers ->
            HeaderUtil.createEntityDeletionAlert(headers, applicationName, true, ENTITY_NAME, id.toString())
    )
  }
}