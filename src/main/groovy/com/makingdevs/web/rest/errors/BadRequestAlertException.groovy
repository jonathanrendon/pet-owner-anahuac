package com.makingdevs.web.rest.errors

import org.zalando.problem.AbstractThrowableProblem
import org.zalando.problem.Status

class BadRequestAlertException extends AbstractThrowableProblem {
  private static final long serialVersionUID = 1L
  private final String entityName
  private final String errorKey

  BadRequestAlertException(String defaultMessage, String entityName, String errorKey) {
    this(ErrorConstants.DEFAULT_TYPE, defaultMessage, entityName, errorKey)
  }

  BadRequestAlertException(URI type, String defaultMessage, String entityName, String errorKey) {
    super(type, defaultMessage, Status.BAD_REQUEST, null, null, null, getAlertParameters(entityName,errorKey))
    this.entityName = entityName
    this.errorKey = errorKey
  }

  String getEntityName() {
    entityName
  }

  String getErrorKey() {
    errorKey
  }

  private static Map<String, Object> getAlertParameters(String entityName, String errorKey) {
    Map<String, Object> parameters = new HashMap<>()
    parameters.put("message", "error. ${errorKey}")
    parameters.put("params", entityName)
    parameters
  }

}
