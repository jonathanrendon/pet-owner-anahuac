package com.makingdevs.web.rest

import com.makingdevs.service.OwnerService
import com.makingdevs.service.dto.OwnerDTO
import com.makingdevs.utils.HeaderUtil
import com.makingdevs.utils.PaginationUtil
import com.makingdevs.web.rest.errors.BadRequestAlertException
import groovy.util.logging.Slf4j
import io.micronaut.context.annotation.Value
import io.micronaut.data.model.Page
import io.micronaut.data.model.Pageable
import io.micronaut.http.HttpRequest
import io.micronaut.http.HttpResponse
import io.micronaut.http.annotation.Body
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Delete
import io.micronaut.http.annotation.Get
import io.micronaut.http.annotation.PathVariable
import io.micronaut.http.annotation.Post
import io.micronaut.http.annotation.Put
import io.micronaut.http.uri.UriBuilder
import io.micronaut.security.annotation.Secured
import io.micronaut.security.rules.SecurityRule
import io.swagger.v3.oas.annotations.security.SecurityRequirement
import io.swagger.v3.oas.annotations.tags.Tag
import jakarta.inject.Inject

@Tag(name = "Owner")
@Slf4j
@Controller("/api")
@Secured(SecurityRule.IS_AUTHENTICATED)
@SecurityRequirement(name = "basicAuth")
class OwnerResource {

  private static final String ENTITY_NAME = "owner"

  @Value('${micronaut.application.name}')
  private String applicationName

  @Inject
  OwnerService ownerService

  @Post("/owners")
  HttpResponse<OwnerDTO> createOwner(@Body OwnerDTO ownerDTO) throws URISyntaxException {
    log.info("REST request to save an Owner: ${ownerDTO}")
    if (ownerDTO.id != null) {
      throw new BadRequestAlertException("A new owner cannot already have an ID", ENTITY_NAME, "idexists")
    }
    OwnerDTO result = ownerService.save(ownerDTO)
    URI location = new URI("/api/owners/${result.id}")
    HttpResponse.created(result).headers( headers -> {
      headers.location(location)
      HeaderUtil.createEntityCreationAlert(headers, applicationName, true, ENTITY_NAME, result.id.toString())
    })
  }

  @Put("/owners")
  HttpResponse<OwnerDTO> updateOwner(@Body OwnerDTO ownerDTO) throws URISyntaxException {
    log.info("REST request to update an Owner: ${ownerDTO}")
    if(ownerDTO.id == null) {
      throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull")
    }
    OwnerDTO result = ownerService.save(ownerDTO)
    HttpResponse.ok(result).headers(headers ->
      HeaderUtil.createEntityUpdateAlert(headers, applicationName, true, ENTITY_NAME, ownerDTO.id.toString())
    )
  }

  @Get("/owners")
  HttpResponse<List<OwnerDTO>> getAllOwners(HttpRequest request, Pageable pageable) {
    log.info("REST request to get a page of Owners")
    Page<OwnerDTO> page = ownerService.findAll(pageable)
    HttpResponse.ok(page.getContent()).headers(headers ->
            PaginationUtil.generatePaginationHttpHeaders(headers, UriBuilder.of(request.getPath()), page)
    )
  }

  @Get("/owners/{id}")
  Optional<OwnerDTO> getOwner(@PathVariable Long id) {
    log.info("REST request to get Owner: ${id}")
    ownerService.findOne(id)
  }

  @Delete("/owners/{id}")
  HttpResponse deleteOwner(@PathVariable Long id) {
    log.info("REST request to delete an Owner: ${id}")
    ownerService.delete(id)
    HttpResponse.noContent().headers(headers ->
      HeaderUtil.createEntityDeletionAlert(headers, applicationName, true, ENTITY_NAME, id.toString())
    )
  }

}
