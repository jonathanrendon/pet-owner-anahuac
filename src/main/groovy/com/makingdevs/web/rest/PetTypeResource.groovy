package com.makingdevs.web.rest

import com.makingdevs.service.PetTypeService
import com.makingdevs.service.dto.PetTypeDTO
import com.makingdevs.utils.HeaderUtil
import com.makingdevs.utils.PaginationUtil
import com.makingdevs.web.rest.errors.BadRequestAlertException
import groovy.util.logging.Slf4j
import io.micronaut.context.annotation.Value
import io.micronaut.data.model.Page
import io.micronaut.data.model.Pageable
import io.micronaut.http.HttpRequest
import io.micronaut.http.HttpResponse
import io.micronaut.http.annotation.Body
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Delete
import io.micronaut.http.annotation.Get
import io.micronaut.http.annotation.PathVariable
import io.micronaut.http.annotation.Post
import io.micronaut.http.annotation.Put
import io.micronaut.http.uri.UriBuilder
import io.micronaut.security.annotation.Secured
import io.micronaut.security.rules.SecurityRule
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.media.ArraySchema
import io.swagger.v3.oas.annotations.media.Content
import io.swagger.v3.oas.annotations.media.Schema
import io.swagger.v3.oas.annotations.responses.ApiResponse
import io.swagger.v3.oas.annotations.security.SecurityRequirement
import io.swagger.v3.oas.annotations.tags.Tag
import jakarta.inject.Inject

@Tag(name = "Pet types")
@Slf4j
@Controller("/api")
@Secured(SecurityRule.IS_AUTHENTICATED)
@SecurityRequirement(name = "basicAuth")
class PetTypeResource {

  private static final  String ENTITY_NAME = "petType"

  @Value('${micronaut.application.name}')
  private  String applicationName

  @Inject
  PetTypeService petTypeService

  @Operation(summary = "Create an pet type")
  @Post("/pet-types")
  @Secured(["ROLE_ADMIN"])
  @ApiResponse(
          responseCode = "200", description = "Pet type created",
          content = @Content(array = @ArraySchema(schema = @Schema(implementation = PetTypeDTO.class)))
  )
  HttpResponse<PetTypeDTO> createPetType(@Body PetTypeDTO petTypeDTO) throws URISyntaxException {
    log.info("REST request to save PetType: ${petTypeDTO}")
    if(petTypeDTO.id != null) {
      throw new  BadRequestAlertException("A new petType cannot already have an ID", ENTITY_NAME, "idexists")
    }
    PetTypeDTO result = petTypeService.save(petTypeDTO)
    URI location = new URI("/api/pet-types/${result.id}")
    HttpResponse.created(result).headers( headers -> {
      headers.location(location)
      HeaderUtil.createEntityCreationAlert(headers, applicationName, true, ENTITY_NAME, result.id.toString())
    })
  }

  @Put("/pet-types")
  HttpResponse<PetTypeDTO> updatePetType(@Body PetTypeDTO petTypeDTO) throws URISyntaxException {
    log.info("REST request to update PetType: ${petTypeDTO}")
    if(petTypeDTO.id == null){
      throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull")
    }
    PetTypeDTO result = petTypeService.save(petTypeDTO)
    HttpResponse.ok(result).headers( headers ->
      HeaderUtil.createEntityUpdateAlert(headers, applicationName, true, ENTITY_NAME, result.id.toString())
    )
  }

  @Get("/pet-types")
  HttpResponse<List<PetTypeDTO>> getAllPetTypes(HttpRequest request, Pageable pageable) {
    log.info("REST request to get a page of PetTypes")
    Page<PetTypeDTO> page = petTypeService.findAll(pageable)
    HttpResponse.ok(page.getContent()).headers( headers ->
            PaginationUtil.generatePaginationHttpHeaders(headers, UriBuilder.of(request.getPath()), page))
  }

  @Get("/pet-types/{id}")
  Optional<PetTypeDTO> getPetType(@PathVariable Long id) {
    log.info("REST request to get a PetType: ${id}")
    petTypeService.findOne(id)
  }

  @Delete("/pet-types/{id}")
  HttpResponse deletePetType(@PathVariable Long id) {
    log.info("REST request to delete PetType: ${id}")
    petTypeService.delete(id)
    HttpResponse.noContent().headers( headers ->
      HeaderUtil.createEntityDeletionAlert(headers, applicationName, true, ENTITY_NAME, id.toString())
    )
  }

}
