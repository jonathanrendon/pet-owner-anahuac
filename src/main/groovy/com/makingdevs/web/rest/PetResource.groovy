package com.makingdevs.web.rest

import com.makingdevs.service.PetService
import com.makingdevs.service.dto.PetDTO
import com.makingdevs.utils.HeaderUtil
import com.makingdevs.utils.PaginationUtil
import com.makingdevs.web.rest.errors.BadRequestAlertException
import groovy.util.logging.Slf4j
import io.micronaut.context.annotation.Value
import io.micronaut.data.model.Page
import io.micronaut.data.model.Pageable
import io.micronaut.http.HttpRequest
import io.micronaut.http.HttpResponse
import io.micronaut.http.annotation.Body
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Delete
import io.micronaut.http.annotation.Get
import io.micronaut.http.annotation.PathVariable
import io.micronaut.http.annotation.Post
import io.micronaut.http.annotation.Put
import io.micronaut.http.uri.UriBuilder
import io.micronaut.security.annotation.Secured
import io.micronaut.security.rules.SecurityRule
import io.swagger.v3.oas.annotations.security.SecurityRequirement
import io.swagger.v3.oas.annotations.tags.Tag

@Tag(name = "Pet")
@Slf4j
@Controller("/api")
@Secured(SecurityRule.IS_AUTHENTICATED)
@SecurityRequirement(name = "basicAuth")
class PetResource {

  private static final String ENTITY_NAME = "pet"

  @Value('${micronaut.application.name}')
  private String applicationName

  private final PetService petService

  PetResource(PetService petService) {
    this.petService = petService
  }

  @Post("/pets")
  HttpResponse<PetDTO> createPet(@Body PetDTO petDTO) throws URISyntaxException {
    log.info("REST request to save Pet : ${petDTO}")
    if (petDTO.getId() != null) {
      throw new BadRequestAlertException("A new pet cannot already have an ID", ENTITY_NAME, "idexists")
    }
    PetDTO result = petService.save(petDTO)
    URI location = new URI("/api/pets/${result.getId()}")
    HttpResponse.created(result).headers(headers -> {
      headers.location(location)
      HeaderUtil.createEntityCreationAlert(headers, applicationName, true, ENTITY_NAME, result.getId().toString())
    })
  }

  @Put("/pets")
  HttpResponse<PetDTO> updatePet(@Body PetDTO petDTO) throws URISyntaxException {
    log.info("REST request to update Pet : ${petDTO}")
    if (petDTO.getId() == null) {
      throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull")
    }
    PetDTO result = petService.save(petDTO)
    HttpResponse.ok(result).headers(headers ->
            HeaderUtil.createEntityUpdateAlert(headers, applicationName, true, ENTITY_NAME, petDTO.getId().toString()))
  }

  @Get("/pets")
  HttpResponse<List<PetDTO>> getAllPets(HttpRequest request, Pageable pageable) {
    log.info("REST request to get a page of Pets")
    Page<PetDTO> page = petService.findAll(pageable)
    HttpResponse.ok(page.getContent()).headers(headers ->
            PaginationUtil.generatePaginationHttpHeaders(headers, UriBuilder.of(request.getPath()), page))
  }

  @Get("/pets/{id}")
  Optional<PetDTO> getPet(@PathVariable Long id) {
    log.debug("REST request to get Pet : ${id}")
    petService.findOne(id)
  }

  @Delete("/pets/{id}")
  HttpResponse deletePet(@PathVariable Long id) {
    log.info("REST request to delete Pet : ${id}")
    petService.delete(id)
    HttpResponse.noContent().headers(headers ->
            HeaderUtil.createEntityDeletionAlert(headers, applicationName, true, ENTITY_NAME, id.toString())
    )
  }

}
