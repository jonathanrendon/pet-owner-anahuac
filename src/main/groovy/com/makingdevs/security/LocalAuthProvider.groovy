package com.makingdevs.security

import groovy.util.logging.Slf4j
import io.micronaut.core.annotation.Nullable
import io.micronaut.http.HttpRequest
import jakarta.inject.Inject
import jakarta.inject.Singleton
import io.micronaut.security.authentication.*
import org.reactivestreams.Publisher
import reactor.core.publisher.Mono

@Slf4j
@Singleton
class LocalAuthProvider implements AuthenticationProvider {

  @Inject
  IdentityStore keyStore

  @Override
  Publisher<AuthenticationResponse> authenticate(HttpRequest httpRequest, AuthenticationRequest authenticationRequest) {
    String username = authenticationRequest.getIdentity().toString()
    String password = authenticationRequest.getSecret().toString()

    log.info("--------------------------------------------------")
    log.info("${username}")
    log.info("${password}")
    log.info("--------------------------------------------------")

    Mono.<AuthenticationResponse>create(emitter -> {
      if(password == keyStore.getUserPassword(username)){
        emitter.success(AuthenticationResponse.success(username,[keyStore.getUserRole(username)]))
      } else {
        emitter.error(AuthenticationResponse.exception())
      }
    })
  }
}
