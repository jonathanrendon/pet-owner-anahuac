package com.makingdevs.repository

import com.makingdevs.domain.PetType
import io.micronaut.data.annotation.Repository
import io.micronaut.data.jpa.repository.JpaRepository

import javax.persistence.EntityManager
import javax.persistence.PersistenceContext
import javax.transaction.Transactional

@SuppressWarnings("unused")
@Repository
abstract class PetTypeRepository implements JpaRepository<PetType, Long>{

  @PersistenceContext
  EntityManager entityManager

  @Transactional
  PetType mergeAndSave(PetType petType){
    petType = entityManager.merge(petType)
    save(petType)
  }


}
