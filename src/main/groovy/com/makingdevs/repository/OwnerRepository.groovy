package com.makingdevs.repository

import com.makingdevs.domain.Owner
import io.micronaut.data.annotation.Repository
import io.micronaut.data.jpa.repository.JpaRepository

import javax.persistence.EntityManager
import javax.persistence.PersistenceContext
import javax.transaction.Transactional

@SuppressWarnings("unused")
@Repository
abstract class OwnerRepository implements JpaRepository<Owner, Long> {

  @PersistenceContext
  EntityManager entityManager

  @Transactional
  Owner mergeAndSave(Owner owner){
    owner = entityManager.merge(owner)
    save(owner)
  }

}
