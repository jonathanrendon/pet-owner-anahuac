package com.makingdevs.repository

import com.makingdevs.domain.Pet
import io.micronaut.data.annotation.Repository
import io.micronaut.data.jpa.repository.JpaRepository

import javax.persistence.EntityManager
import javax.transaction.Transactional

@SuppressWarnings("unused")
@Repository
abstract class PetRepository implements JpaRepository<Pet, Long> {

  private final EntityManager entityManager

  PetRepository(EntityManager entityManager) {
    this.entityManager = entityManager
  }

  @Transactional
  Pet mergeAndSave(Pet pet) {
    pet = entityManager.merge(pet)
    save(pet)
  }

}
