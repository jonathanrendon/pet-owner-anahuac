package com.makingdevs.repository

import com.makingdevs.domain.Visit
import io.micronaut.data.annotation.Repository
import io.micronaut.data.jpa.repository.JpaRepository

import javax.persistence.EntityManager
import javax.persistence.PersistenceContext
import javax.transaction.Transactional

@SuppressWarnings("unused")
@Repository
abstract class VisitRepository implements JpaRepository<Visit, Long>{

  @PersistenceContext
  EntityManager entityManager

  @Transactional
  Visit mergeAndSave(Visit visit) {
    visit = entityManager.merge(visit)
    save(visit)
  }
}
