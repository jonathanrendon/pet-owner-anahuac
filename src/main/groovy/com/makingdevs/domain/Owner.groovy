package com.makingdevs.domain

import javax.persistence.CascadeType
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.OneToMany
import javax.persistence.Table

@Entity
@Table(name = "owners", schema = "petowner")
class Owner implements Serializable {

  static final long serialVersionUID = 1L

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  Long id

  @Column(name = "first_name")
  String firstName

  @Column(name = "last_name")
  String lastName

  @Column(name = "address")
  String address

  @Column(name = "city")
  String city

  @Column(name = "telephone")
  String telephone

  @OneToMany(mappedBy = "owner", orphanRemoval = true, cascade = CascadeType.ALL)
  Set<Pet> pets = new HashSet<>()

  @Override
  String toString() {
    """Owner{
          id=${getId()},
          firstName=${getFirstName()},
          lastName=${getLastName()},
          address=${getAddress()},
          city=${getCity()},
          telephone=${getTelephone()}
    }"""
  }

}
