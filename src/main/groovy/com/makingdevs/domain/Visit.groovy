package com.makingdevs.domain

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.Table
import java.time.LocalDate

@Entity
@Table(name = "visits", schema = "petowner")
class Visit implements Serializable{

  static final long serialVersionUID = 1L

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  Long id

  @Column(name = "visit_date")
  LocalDate visitDate

  @Column(name = "description")
  String description

  @ManyToOne
  @JoinColumn(name = "pet_id")
  Pet pet

  @Override
  String toString() {
    """Visit{
        id=${getId()},
        visitDate=${getVisitDate()},
        description=${getDescription()}
    }"""
  }

}
