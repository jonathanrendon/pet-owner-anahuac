package com.makingdevs.domain

import groovy.transform.ToString

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name="types", schema = "petowner")
class PetType implements  Serializable{

  static final long serialVersionUID = 1L

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  Long id

  @Column(name="name")
  String name

  @Override
  String toString() {
    """PetType{ 
        id=${id}, 
        name= ${name} 
    }"""
  }
}
