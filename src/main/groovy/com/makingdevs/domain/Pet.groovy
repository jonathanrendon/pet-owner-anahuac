package com.makingdevs.domain

import javax.persistence.CascadeType
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.OneToMany
import javax.persistence.Table
import java.time.LocalDate

@Entity
@Table(name = "pets", schema = "petowner")
class Pet implements Serializable {
  static final long serialVersionUID = 1L

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  Long id

  @Column(name = "name")
  String name

  @Column(name = "birth_date")
  LocalDate birthDate

  @ManyToOne
  @JoinColumn(name = "owner_id")
  Owner owner

  @OneToMany(mappedBy = "pet", orphanRemoval = true, cascade = CascadeType.ALL)
  Set<Visit> visits = new HashSet<>()

  @ManyToOne
  @JoinColumn(name = "type_id")
  PetType type

  @Override
  String toString() {
    """Pet{
      id=${id},
      name=${getName()}
      birthDate=${birthDate}
    }"""
  }
}
