package com.makingdevs.web

import com.makingdevs.domain.PetType
import com.makingdevs.service.dto.OwnerDTO
import com.makingdevs.service.dto.PetDTO
import com.makingdevs.service.dto.VisitDTO
import groovy.util.logging.Slf4j
import io.micronaut.http.HttpRequest
import io.micronaut.http.HttpResponse
import io.micronaut.http.client.HttpClient
import io.micronaut.http.client.annotation.Client
import io.micronaut.http.client.exceptions.HttpClientResponseException
import io.micronaut.test.extensions.spock.annotation.MicronautTest
import jakarta.inject.Inject
import spock.lang.Specification


import static io.micronaut.http.HttpStatus.UNAUTHORIZED
import static io.micronaut.http.HttpStatus.OK

@Slf4j
@MicronautTest
class BasicAuthSpec extends Specification{

  @Inject
  @Client("/")
  HttpClient httpClient

  void "Por defecto casi todos los endpoints tiene seguridad"() {
    when: "Intento accesar a una url segura sin autenticarme"
    httpClient.toBlocking().exchange(HttpRequest.GET('api/owners'))
    httpClient.toBlocking().exchange(HttpRequest.GET('api/visits'))
    httpClient.toBlocking().exchange(HttpRequest.GET('api/petTypes'))
    httpClient.toBlocking().exchange(HttpRequest.GET('api/pets'))

    then: "debe de retornar respuesta de acceso no autorizado"
    HttpClientResponseException e = thrown()
    log.info("----------------------------")
    log.info("Message: ${e.message}")
    log.info("----------------------------")
    e.status == UNAUTHORIZED
  }

  void "Verifica la seguridad basica http funciona Owners"() {
    when: "Intento accesar a una url segura"
    HttpRequest request = HttpRequest.GET('api/owners').basicAuth("bob","bob@2")
    HttpResponse<String> response = httpClient.toBlocking().exchange(request, String)
    OwnerDTO[] owners = httpClient.toBlocking().retrieve(request, OwnerDTO[].class)

    then: "Validamos estatus ok y contenido de petición"
    log.info("----------------------------")
    log.info("Message: ${httpClient.dump()}")
    log.info("Message: ${owners.inspect()}")
    log.info("----------------------------")
    response.status == OK
  }

  void "Verifica la seguridad basica http funciona Pets"() {
    when: "Intento accesar a una url segura"
    HttpRequest request = HttpRequest.GET('api/pets').basicAuth("bob","bob@2")
    HttpResponse<String> response = httpClient.toBlocking().exchange(request, String)
    PetDTO[] pets = httpClient.toBlocking().retrieve(request, PetDTO[].class)

    then: "Validamos estatus ok y contenido de petición"
    log.info("----------------------------")
    log.info("Message: ${httpClient.dump()}")
    log.info("Message: ${pets.inspect()}")
    log.info("----------------------------")
    response.status == OK
  }

  void "Verifica la seguridad basica http funciona PetTypes"() {
    when: "Intento accesar a una url segura"
    HttpRequest request = HttpRequest.GET('api/petTypes').basicAuth("bob","bob@2")
    HttpResponse<String> response = httpClient.toBlocking().exchange(request, String)
    PetType[] petTypes = httpClient.toBlocking().retrieve(request, PetType[].class)

    then: "Validamos estatus ok y contenido de petición"
    log.info("----------------------------")
    log.info("Message: ${httpClient.dump()}")
    log.info("Message: ${petTypes.inspect()}")
    log.info("----------------------------")
    response.status == OK
  }

  void "Verifica la seguridad basica http funciona Visits"() {
    when: "Intento accesar a una url segura"
    HttpRequest request = HttpRequest.GET('api/visits').basicAuth("bob","bob@2")
    HttpResponse<String> response = httpClient.toBlocking().exchange(request, String)
    VisitDTO[] visits = httpClient.toBlocking().retrieve(request, VisitDTO[].class)

    then: "Validamos estatus ok y contenido de petición"
    log.info("----------------------------")
    log.info("Message: ${httpClient.dump()}")
    log.info("Message: ${visits.inspect()}")
    log.info("----------------------------")
    response.status == OK
  }
}
