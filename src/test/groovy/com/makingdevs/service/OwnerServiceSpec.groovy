package com.makingdevs.service

import com.makingdevs.domain.Owner
import com.makingdevs.service.OwnerService
import com.makingdevs.service.dto.OwnerDTO
import groovy.util.logging.Slf4j
import io.micronaut.test.extensions.spock.annotation.MicronautTest
import jakarta.inject.Inject
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Unroll

@Slf4j
@MicronautTest(transactional = false)
@Unroll
class OwnerServiceSpec extends Specification{

  @Shared
  @Inject
  OwnerService ownerService

  @Shared
  OwnerDTO ownerDTO

  void setupSpec(){
    ownerDTO = new OwnerDTO(firstName: "Eduardo", lastName: "Morales", city: "Nowhere", address: "Nowhere", telephone: "55-55-55-55")
  }

  void "Deberia consultar un par de Owners"() {
    given:
    Long id = _id

    when:
    Optional<OwnerDTO> optionalOwner = ownerService.findOne(id)

    then:
    optionalOwner.ifPresent(ow -> log.info("OwnerDTO:  ${ow.dump()}"))
    optionalOwner.ifPresent(ow -> ow.firstName == _firstName)
    optionalOwner.get().class == OwnerDTO.class

    where:
    _id     || _firstName
    8L      || "Maria"
    10L     || "Carlos"
  }

  void "Deberia crera un nuevo Owner"() {
    given:
    OwnerDTO ownerDTOCreated = _owner

    when:
    ownerDTOCreated = ownerService.save(ownerDTOCreated)

    then:
    log.info("-------------------------------")
    log.info("Owner creado ${ownerDTOCreated}")
    log.info("-------------------------------")
    ownerDTOCreated.id
    ownerDTOCreated.class == OwnerDTO.class
    ownerDTOCreated.lastName == _lastName

    where:
    _owner      || _lastName
    ownerDTO    || ownerDTO.lastName
  }

}
