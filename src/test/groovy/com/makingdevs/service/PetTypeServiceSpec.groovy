package com.makingdevs.service

import com.makingdevs.domain.PetType
import com.makingdevs.service.dto.PetTypeDTO
import groovy.util.logging.Slf4j
import io.micronaut.test.extensions.spock.annotation.MicronautTest
import jakarta.inject.Inject
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Unroll

@Slf4j
@MicronautTest(transactional = false)
@Unroll
class PetTypeServiceSpec extends Specification{

  @Shared
  @Inject
  PetTypeService petTypeService

  @Shared
  List<PetTypeDTO> petTypes

  void setupSpec() {
    petTypes = [new PetTypeDTO(name: "Halcon"), new PetTypeDTO(name: "Coyote")]
  }

  void "Deberia consultar una lista de PetTypes"() {
    given:
    Long id = _id

    when:
    Optional<PetTypeDTO> optionalPetType = petTypeService.findOne(id)

    then:
    optionalPetType.ifPresent(pt -> log.info("PetTypeDTO ${pt.dump()}"))
    optionalPetType.ifPresent(pt -> pt.name == _name)
    optionalPetType.get().class == PetTypeDTO.class

    where:
    _id || _name
    1L  || "cat"
    4L  || "Snake"
  }

  void "Deberia crear PetTypes desde una lista ya establecida"() {
    given:
    PetTypeDTO petTypeDTOCreated = _petType

    when:
    petTypeDTOCreated = petTypeService.save(petTypeDTOCreated)

    then:
    log.info("-------------------------------")
    log.info("Pet type creado ${petTypeDTOCreated}")
    log.info("-------------------------------")
    petTypeDTOCreated.id
    petTypeDTOCreated.class == PetTypeDTO.class
    petTypeDTOCreated.name == _result

    where:
    _petType          || _result
    petTypes.get(0)   || petTypes.get(0).name
    petTypes.get(1)   || petTypes.get(1).name
  }

}
