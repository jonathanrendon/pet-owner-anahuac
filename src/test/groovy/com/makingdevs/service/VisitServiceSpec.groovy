package com.makingdevs.service

import com.makingdevs.domain.Visit
import com.makingdevs.service.dto.VisitDTO
import groovy.util.logging.Slf4j
import io.micronaut.test.extensions.spock.annotation.MicronautTest
import jakarta.inject.Inject
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Unroll

import java.time.LocalDate

@Slf4j
@MicronautTest(environments = ["dblocal"],rollback = false)
@Unroll
class VisitServiceSpec extends Specification{

  @Shared
  @Inject
  VisitService visitService

  @Shared
  @Inject
  PetService petService

  @Shared
  VisitDTO visitDTO

  void setupSpec(){
    visitDTO = new VisitDTO(description: "Visit to Lucky",
            visitDate: LocalDate.now(), petId: petService.findOne(9L).get().id)
  }

  void "Deberia recuperar un par de Visits"() {
    given:
    Long id = _id

    when:
    Optional<VisitDTO> optionalVisit = visitService.findOne(id)

    then:
    optionalVisit.ifPresent(vt -> log.info("VisitDTO ${vt.dump()}"))
    optionalVisit.get().description == _result
    optionalVisit.get().class == VisitDTO.class

    where:
    _id   || _result
    1L    || "rabies shot"
    3L    || "neutered"
  }

  void "Deberia crear un nuevo Visit"() {
    given:
    VisitDTO visitDTOCreated = _visit

    when:
    visitDTOCreated = visitService.save(visitDTOCreated)

    then:
    log.info("-------------------------------")
    log.info("VisitDTO creado ${visitDTOCreated}")
    log.info("-------------------------------")
    visitDTOCreated.id
    visitDTOCreated.class == VisitDTO.class
    visitDTOCreated.description == _description

    where:
    _visit       || _description
    visitDTO     || visitDTO.description
  }

}
