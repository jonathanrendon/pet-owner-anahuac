package com.makingdevs.service

import com.makingdevs.domain.Pet
import com.makingdevs.service.dto.PetDTO
import com.makingdevs.service.mapper.PetTypeMapper
import groovy.util.logging.Slf4j
import io.micronaut.test.extensions.spock.annotation.MicronautTest
import jakarta.inject.Inject
import spock.lang.Shared
import spock.lang.Specification

import java.time.LocalDate

@Slf4j
@MicronautTest(environments = ["dblocal"], rollback = false)
class PetServiceSpec extends Specification{

  @Shared
  @Inject
  PetService petService

  @Shared
  @Inject
  OwnerService ownerService

  @Shared
  @Inject
  PetTypeService petTypeService

  @Shared
  PetDTO petDTO

  void setupSpec() {
    petDTO = new PetDTO(name: "Piolin", birthDate: LocalDate.now(),
          type: petTypeService.findOne(5L).get(),
          ownerId: ownerService.findOne(6L).get().id)
  }

  void "Debería recuperar un par de Pets"() {
    given:
    Long id = _id

    when:
    Optional<PetDTO> optionalPet = petService.findOne(id)

    then:
    optionalPet.ifPresent(pt -> log.info("PetDTO ${pt.dump()}"))
    optionalPet.get().name == _name
    optionalPet.get().class == PetDTO.class

    where:
    _id   ||  _name
    1L    ||  "Leo"
    8L    ||  "Max"
  }

  void "Debería crear un nuevo Pet" () {
    given:
    PetDTO petDTOCreated = _pet

    when:
    petDTOCreated = petService.save(petDTOCreated)

    then:
    log.info("-------------------------------")
    log.info("Pet creado ${petDTOCreated}")
    log.info("-------------------------------")
    petDTOCreated.id
    petDTOCreated.class == PetDTO.class
    petDTOCreated.name == _name

    where:

    _pet    ||  _name
    petDTO  || petDTO.name
  }



}
