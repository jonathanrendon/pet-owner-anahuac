package com.makingdevs.repository

import com.makingdevs.domain.PetType
import groovy.util.logging.Slf4j
import io.micronaut.data.model.Pageable
import io.micronaut.test.extensions.spock.annotation.MicronautTest
import jakarta.inject.Inject
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Stepwise

@Slf4j
@MicronautTest(rollback = false)
@Stepwise
class PetTypeRepositorySpec extends Specification{

  @Inject
  PetTypeRepository petTypeRepository

  @Shared
  PetType petType

  void setupSpec() {
    log.info("---------------- Hello Spec ---------------------")
    petType = new PetType()
  }

  void "1 Debería crear un registro de tipo pet type" () {
    when: "Se inicializa un objeto de tipo PetType y se invoca la preación por medio del repositorio"
    petType.name = "Dragon"
    petType = petTypeRepository.mergeAndSave(petType)

    then: "El PetType Dragon debió de crearse"
    log.info("-------------------------------")
    log.info("${petType.toString()}")
    log.info("-------------------------------")
    petType.id
    petType.name == "Dragon"
  }

  void "2 Debería recuperar Pet type Dragon creado" () {
    when: "Invocamos al repositorio para consultar un tipo de pet type por id"
    log.info("Pet type init ${petType.toString()}")
    PetType petTypeFromRepository = petTypeRepository.findById(petType.id).get()

    then: "El tipo Dragon debería ser recuperado"
    log.info("-------------------------------")
    log.info("${petTypeRepository.toString()}")
    log.info("-------------------------------")
    petTypeFromRepository.id == petType.id
    petTypeFromRepository.name == petType.name
  }

  void "3 Deberia regresar la lista completa de pet types" () {
    when: "Se invoca repositorio para recuperar todos los pet types"
    List<PetType> petTypes = petTypeRepository.findAll(Pageable.from(0,7)).getContent()

    then: "Todos los pet types deberían ser regresados"
    log.info("-------------------------------")
    log.info("${petTypes.dump()}")
    log.info("-------------------------------")
    petTypes.size() == 7
  }

  void "4 Deberia actualizar pet type Dragon" () {
    when: "Se invoca repositorio para actualizar pet type"
    petType.name = "Dragon Fly"
    PetType petTypeUpdated = petTypeRepository.update(petType)

    then: "Valida pet type actualizado"
    log.info("-------------------------------")
    log.info("Pet type updated ${petTypeUpdated.toString()}")
    log.info("-------------------------------")
    petTypeUpdated.id == petType.id
    petTypeUpdated.name == petType.name
  }

  void "5 Deberia eliminar el registro pet type Dragon Fly" (){
    when: "Se invoca repositorio para eliminar pet type"
    List<PetType> petTypes = petTypeRepository.findAll(Pageable.UNPAGED).getContent()
    log.info("-------------------------------")
    log.info("Pet types antes de borrado ${petTypes.dump()}")
    log.info("-------------------------------")
    petTypeRepository.deleteById(petType.id)
    petTypes = petTypeRepository.findAll(Pageable.UNPAGED).getContent()

    then: "Verificamos que pet type ha sido eliminado"
    log.info("-------------------------------")
    log.info("Pet types despues de borrado${petTypes}")
    log.info("-------------------------------")
    !petTypeRepository.findById(petType.id).isPresent()
  }
}
