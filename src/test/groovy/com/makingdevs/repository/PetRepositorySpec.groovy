package com.makingdevs.repository

import com.makingdevs.domain.Pet
import groovy.util.logging.Slf4j
import io.micronaut.data.model.Pageable
import io.micronaut.test.extensions.spock.annotation.MicronautTest
import jakarta.inject.Inject
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Stepwise

import javax.transaction.Transactional
import java.time.LocalDate

@Slf4j
@MicronautTest(rollback = false)
@Stepwise
class PetRepositorySpec extends Specification{

  @Shared
  @Inject
  PetRepository petRepository

  @Shared
  @Inject
  PetTypeRepository petTypeRepository

  @Shared
  @Inject
  OwnerRepository ownerRepository

  @Shared
  Pet pet

  @Shared
  int petsSize

  void setupSpec() {
    petsSize = petRepository.findAll().size()
    pet = new Pet(name: "Piolin",
            birthDate: LocalDate.now(), type: petTypeRepository.findById(5L).get(),
            owner: ownerRepository.findAll().get(1))
  }

  void "1Deberia crear un nuevo pet"() {
    when: "Se invoca repositorio para crear un nuevo pet"
    pet = petRepository.mergeAndSave(pet)

    then: "Pet Piolin debió ser creado"
    log.info("-------------------------------")
    log.info("Pet creado ${pet.toString()}")
    log.info("-------------------------------")
    pet.id
    pet.name == "Piolin"
  }

  void "2 Debería recuperar un pet"() {
    when:
    Pet petFromRepository = petRepository.findById(pet.id).get()

    then:
    log.info("-------------------------------")
    log.info("Pet consultado ${petFromRepository.toString()}")
    log.info("-------------------------------")
    petFromRepository.id == pet.id
    petFromRepository.name == pet.name
  }

  void "3 Debería recuperar todos los pet"() {
    when:
    List<Pet> pets = petRepository.findAll(Pageable.UNPAGED).getContent()

    then:
    log.info("-------------------------------")
    log.info("Lista de pets ${pets.dump()}")
    log.info("-------------------------------")
    pets.size() == petsSize + 1
  }

  void "4 Debería actualizar un pet"() {
    when:
    pet.name = "Claudio"
    Pet petUpdated = petRepository.update(pet)

    then:
    log.info("-------------------------------")
    log.info("Pet actualizado ${petUpdated.dump()}")
    log.info("-------------------------------")
    petUpdated.name == pet.name
    petUpdated.id == pet.id
  }

  void "5 Debería eliminar un pet"() {
    when:
    log.info("Lista de pets antes de eliminar ${petRepository.findAll().dump()}")
    petRepository.deleteById(pet.id)

    then:
    log.info("Lista de pets despues de eliminar${petRepository.findAll().dump()}")
    petsSize == petsSize
    !petRepository.findById(pet.id).isPresent()
  }
}
