package com.makingdevs.repository

import com.makingdevs.domain.Visit
import groovy.util.logging.Slf4j
import io.micronaut.data.model.Pageable
import io.micronaut.test.extensions.spock.annotation.MicronautTest
import jakarta.inject.Inject
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Stepwise

import java.time.LocalDate

@Slf4j
@MicronautTest(rollback = false)
@Stepwise
class VisitRepositorySpec extends Specification{

  @Shared
  @Inject
  VisitRepository visitRepository

  @Shared
  @Inject
  PetRepository petRepository

  @Shared
  Visit visit

  @Shared
  int visitsSize

  void setupSpec() {
    visitsSize = visitRepository.findAll().size()
    visit = new Visit(description: "Visitando a Sly", visitDate: LocalDate.now(), pet: petRepository.findAll().get(0))
  }

  void "1 Deberia crear un visit"() {
    when:
    visit = visitRepository.mergeAndSave(visit)

    then:
    log.info("-------------------------------")
    log.info("Visit creado ${visit.toString()}")
    log.info("-------------------------------")
    visit.id
    visit.description == "Visitando a Sly"
  }

  void "2 Deberia recuperar un visit"() {
    when:
    Visit visitFromRepository = visitRepository.findById(visit.id).get()

    then:
    log.info("-------------------------------")
    log.info("Visit consultado ${visitFromRepository.toString()}")
    log.info("-------------------------------")
    visitFromRepository.id == visit.id
    visitFromRepository.description == visit.description
  }

  void "3 Deberia recuperar todos los visits"() {
    when:
    List<Visit> visits = visitRepository.findAll(Pageable.UNPAGED).getContent()

    then:
    log.info("-------------------------------")
    log.info("Lista de visits ${visits.dump()}")
    log.info("-------------------------------")
    visits.size() == visitsSize + 1
  }

  void "4 Debería actualizar un visit"() {
    when:
    visit.description = "Visit to other"
    Visit visitUpdated = visitRepository.update(visit)

    then:
    log.info("-------------------------------")
    log.info("Visit actulizado ${visit.toString()}")
    log.info("-------------------------------")
    visitUpdated.id == visit.id
    visitUpdated.description == visit.description
  }

  void "5 Debería eliminar un visit"() {
    when:
    log.info("Visit antes de borrado ${visitRepository.findAll().dump()}")
    visitRepository.deleteById(visit.id)

    then:
    log.info("Visit despues de borrado ${visitRepository.findAll().dump()}")
    visitsSize == visitRepository.findAll().size()
    !visitRepository.findById(visit.id).isPresent()
  }
}
