package com.makingdevs.repository

import com.makingdevs.domain.Owner
import groovy.util.logging.Slf4j
import io.micronaut.data.model.Pageable
import io.micronaut.test.extensions.spock.annotation.MicronautTest
import jakarta.inject.Inject
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Stepwise

@Slf4j
@MicronautTest(rollback = false)
@Stepwise
class OwnerRepositorySpec extends Specification{

  @Shared
  @Inject
  OwnerRepository ownerRepository

  @Shared
  Owner owner

  @Shared
  int ownersSize

  void setupSpec() {
    ownersSize = ownerRepository.findAll(Pageable.UNPAGED).getContent().size()
    owner = new Owner(firstName: "Laslo", lastName: "Losla", city: "Nowhere", address: "Nowhere", telephone: "55-55-55-55")
  }

  void "1 Deberia crear un nuevo owner"() {
    when: "Se invoca repositorio para crear un nuevo owner"
    log.info("Owner antes de ser persistido ${owner.toString()}")
    owner = ownerRepository.mergeAndSave(owner)

    then:
    log.info("-------------------------------")
    log.info("Owner creado ${owner.toString()}")
    log.info("-------------------------------")
    owner.id
  }

  void "2 Deberia recuperar un owner"() {
    when: "Se invoca repositorio para consultar un owner"
    Owner ownerFromRepository = ownerRepository.findById(owner.id).get()

    then: "Laslo losla debería ser recuperado"
    ownerFromRepository.id == owner.id
    ownerFromRepository.lastName == owner.lastName
  }

  void "3 Debería recuperar todos los owners"() {
    when: "Cuando invocamos repositorio para recuperar todos los owners"
    List<Owner> owners = ownerRepository.findAll(Pageable.UNPAGED).getContent()

    then: "El total de owners recuperados debería ser 10"
    owners.size() == ownersSize+1
  }

  void "4 Debería actualizar un owner"() {
    when: "Actualizamos datos a owner e invocamos el repositorio para actulizar un owner"
    owner.lastName = "Matute"
    Owner ownerUpdated = ownerRepository.update(owner)

    then: "El nombre del owner debería ser actulizado"
    log.info("-------------------------------")
    log.info("Owner actualizado ${ownerUpdated.toString()}")
    log.info("-------------------------------")
    ownerUpdated.lastName == owner.lastName
    ownerUpdated.id == owner.id
  }

  void "5 Debería eliminar un owner"() {
    when: "Invocamos el repositorio para eliminar un owner"
    ownerRepository.deleteById(owner.id)
    int ownersSizeAfterDelete = ownerRepository.findAll().size()

    then: "El numero de owners deberia ser 10"
    ownersSizeAfterDelete == ownersSize
    !ownerRepository.findById(owner.id).isPresent()
  }

}
